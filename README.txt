This is a quick and dirty app that takes a set of pictures and produces a youtube commercial that relates to the picture.


Directions for fedora(Linux) system
1)Install python (usually already installed) 
  A)sudo yum install python
2)Install iqengine python client
  A)git clone https://github.com/iqengines/pyiqe/
  B)cd pyiqe;sudo python setup.py install
3)Install google-data client
  A)wget http://gdata-python-client.googlecode.com/files/gdata-2.0.15.zip
  B)unzip gdata-2.0.15.zip
  C)cd gdata-2.0.15;sudo python setup.py install
4) Create an account and get a developer key from iqengines.com
5) Get a YOUTUBE developer key
6) Define those keys in a file keys.py like so:
   YOUTUBE_KEY=''
   IQE_KEY=''
   IQE_SECRET=''

5)Run the program from where it is installed
  A)python submitter.py [image folder]
