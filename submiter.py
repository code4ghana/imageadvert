from pyiqe import Api
import time
import gdata.youtube
import gdata.youtube.service
import os

def getKeyWords(folder,api):
    path=os.path.abspath(folder)
    files=os.listdir(path)

    for f in files:
        loc=os.path.join(folder, f)
        while True:
            try:
                data, qid=api.query(loc)
                result=api.update()
                res=result['data']['results'][0]['qid_data']['labels']
                url,title=youtubeSearch(res+' commercial')
                yield (tuple([f,res,title,url]))
                time.sleep(2)
                break;
            except :
                print "server not responding pausing..."
                time.sleep(10)
                pass
                      
def youtubeSearch(search_terms):
     yt_service = gdata.youtube.service.YouTubeService()
     query = gdata.youtube.service.YouTubeVideoQuery()
     query.vq = search_terms
     query.max_results=1
     query.orderby = 'relevance'
     feed = yt_service.YouTubeQuery(query)
     return feed.entry[0].GetSwfUrl(),feed.entry[0].media.title.text
    
        
if __name__=="__main__":
    import sys
    if len(sys.argv)<2:
        print "Please enter the name of your images folder"
        exit()
    YOUTUBE_KEY=''
    IQE_KEY=''
    IQE_SECRET=''
    try:
        from keys import *
    except:
        pass
    if not all(map(len,[YOUTUBE_KEY,IQE_SECRET,IQE_KEY])):
        print "A Key is missing"
        exit()
    yt_service = gdata.youtube.service.YouTubeService()    
    api=Api(IQE_KEY,IQE_SECRET)
    with open("results.txt","w") as f:
        for  group in getKeyWords(sys.argv[1],api):
            s="%s  --   %s --  %s --  %s \n"%group
            print s
            f.write(s)
